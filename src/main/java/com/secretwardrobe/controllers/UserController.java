package com.secretwardrobe.controllers;


import com.secretwardrobe.model.User;
import com.secretwardrobe.model.responses.Response;
import com.secretwardrobe.model.responses.ResponseFactory;
import com.secretwardrobe.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    UserService service;


    @RequestMapping(value = "/testUser", method = RequestMethod.GET)
    public ResponseEntity<User> requestTestUser() {
        return new ResponseEntity<User>(
                new User("login", "hash"), HttpStatus.OK);
    }

    @RequestMapping(value = "/registerUser", method = RequestMethod.GET)
    public ResponseEntity<Response> registerUser(@RequestParam String userName,
                                                 @RequestParam String password) {
        Logger.getLogger(getClass()).debug("Requested registerUser with params:" +
                " " + userName + ":" + password);


        // TODO: sprawdź czy użytkownik istnieje
        // TODO: jeśli nie istnieje użytkownik o podanym loginie, zarejestruj go
        if (service.userExists(userName)) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User exists"),
                    HttpStatus.BAD_REQUEST);
        } else {
            service.registerUser(new User(userName, password));
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/listUsers", method = RequestMethod.GET)
    public ResponseEntity<List<User>> requestUserList() {
        return new ResponseEntity<List<User>>(service.getAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> getUserWithId(@PathVariable Long id) {
        Optional<User> user = service.getUserWithId(id);
        if (user.isPresent()) {
            return new ResponseEntity<Response>(ResponseFactory.success(user.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exist."), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/userExists/{login}", method = RequestMethod.GET)
    public ResponseEntity<Response> requestUserExists(@PathVariable String login) {
        return new ResponseEntity<Response>(ResponseFactory.success(service.userExists(login)), HttpStatus.OK);
    }


    @RequestMapping(value = "/addTransaction/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> executeTransaction(@PathVariable Long id,
                                                       @RequestParam Long otherUserId,
                                                       @RequestParam Double amount) {
        service.addTransaction(id, otherUserId, amount);
        return new ResponseEntity<Response>(ResponseFactory.success(), HttpStatus.OK);
    }
}