package com.secretwardrobe.services;

import com.secretwardrobe.model.Product;

import java.util.List;

public interface ProductService {
    // chcemy dodać produkt :
    boolean addProduct (Product productToAdd);

    //chcemy wyszukać produkt po nazwie :
   List<Product> findByName ( String name);

    // chcemy edytować produkt :

    boolean editProduct ( Product product);

    // wymiana produktu:

    boolean exchangeProduct (Product product);

}
