package com.secretwardrobe.dao;

import com.secretwardrobe.model.Transaction;
import com.secretwardrobe.model.User;
import com.sun.istack.internal.NotNull;

import java.util.Optional;

public interface UserDao {
    void addUser(User u);

    Optional<User> findById(@NotNull Long id);

    boolean userExists(@NotNull String withLogin);

    void persistTransaction(@NotNull User user, @NotNull Transaction transaction);
}